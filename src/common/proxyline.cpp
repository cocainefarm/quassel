#include "proxyline.h"

/**
 * Extracts a space-delimited fragment
 * @param raw Raw Message
 * @param start Current index into the message, will be advanced automatically
 * @param end End of fragment, if already known. Default is -1, in which case it will be set to the next whitespace
 * character or the end of the string
 * @param prefix Required prefix. Default is 0. If set, this only parses a fragment if it starts with the given prefix.
 * @return Fragment
 */
QByteArray extractFragment(const QByteArray& raw, int& start, int end = -1)
{
    // Try to set find the end of the space-delimited fragment
    if (end == -1) {
        end = raw.indexOf(' ', start);
    }
    // If no space comes after this point, use the remainder of the string
    if (end == -1) {
        end = raw.length();
    }
    QByteArray fragment = raw.mid(start, end - start);
    start = end;
    return fragment;
}

ProxyLine ProxyLine::parseProxyLine(const QByteArray& line)
{
    /**
     * Skips empty parts in the message
     * @param raw Raw Message
     * @param start Current index into the message, will be advanced  automatically
     */
    auto skipEmptyParts = [](const QByteArray& raw, int& start) {
        while (start < raw.length() && raw[start] == ' ') {
            start++;
        }
    };

    ProxyLine result;

    int start = 0;
    if (line.startsWith("PROXY")) {
        start = 5;
    }
    skipEmptyParts(line, start);
    QByteArray protocol = extractFragment(line, start);
    if (protocol == "TCP4") {
        result.protocol = QAbstractSocket::IPv4Protocol;
    }
    else if (protocol == "TCP6") {
        result.protocol = QAbstractSocket::IPv6Protocol;
    }
    else {
        result.protocol = QAbstractSocket::UnknownNetworkLayerProtocol;
    }

    if (result.protocol != QAbstractSocket::UnknownNetworkLayerProtocol) {
        bool ok;
        skipEmptyParts(line, start);
        result.sourceHost = QHostAddress(QString::fromLatin1(extractFragment(line, start)));
        skipEmptyParts(line, start);
        result.sourcePort = QString::fromLatin1(extractFragment(line, start)).toUShort(&ok);
        if (!ok)
            result.sourcePort = 0;
        skipEmptyParts(line, start);
        result.targetHost = QHostAddress(QString::fromLatin1(extractFragment(line, start)));
        skipEmptyParts(line, start);
        result.targetPort = QString::fromLatin1(extractFragment(line, start)).toUShort(&ok);
        if (!ok)
            result.targetPort = 0;
    }

    return result;
}

QDebug operator<<(QDebug dbg, const ProxyLine& p)
{
    dbg.nospace();
    dbg << "(protocol = " << p.protocol;
    if (p.protocol == QAbstractSocket::UnknownNetworkLayerProtocol) {
        dbg << ")";
    }
    else {
        dbg << ", sourceHost = " << p.sourceHost << ", sourcePort = " << p.sourcePort << ", targetHost = " << p.targetHost
            << ", targetPort = " << p.targetPort << ")";
    }
    return dbg.space();
}
