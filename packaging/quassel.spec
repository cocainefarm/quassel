Name:           quassel
Version:        0.14.0
Release:        1%{?dist}
Summary:        Quassel IRC: Chat comfortably. Everywhere.

License:        GPLv3
URL:            https://github.com/quassel/quassel/
Source0:        %{name}-%{version}.tar

BuildRequires: cmake
BuildRequires: dbusmenu-qt5-devel
BuildRequires: desktop-file-utils
BuildRequires: extra-cmake-modules
BuildRequires: kf5-kconfigwidgets-devel
BuildRequires: kf5-kcoreaddons-devel
BuildRequires: kf5-knotifications-devel
BuildRequires: kf5-knotifyconfig-devel
BuildRequires: kf5-ktextwidgets-devel
BuildRequires: kf5-kwidgetsaddons-devel
BuildRequires: kf5-kwindowsystem-devel
BuildRequires: kf5-kxmlgui-devel
BuildRequires: kf5-rpm-macros
BuildRequires: boost-devel
BuildRequires: openssl-devel
BuildRequires: perl-generators
BuildRequires: phonon-qt5-devel
BuildRequires: qca-qt5-devel
BuildRequires: qt5-linguist
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtscript-devel
BuildRequires: qt5-qtwebkit-devel
BuildRequires: qt5-qtmultimedia-devel
BuildRequires: openldap-devel


%description
Quassel IRC is a modern, cross-platform, distributed IRC client, meaning that
one (or multiple) client(s) can attach to and detach from a central core --
much like the popular combination of screen and a text-based IRC client such
as WeeChat, but graphical.

Not only do we aim to bring a pleasurable, comfortable chatting experience to
all major platforms, but it's free - as in beer and as in speech, since we
distribute Quassel under the GPL, and you are welcome to download and see for
yourself!


%package common
Summary: Quassel common/shared files
%description common
%{summary}.

%package common-lib
Summary: Quassel common-lib/shared files
%description common-lib
%{summary}.

%package core
Summary: Quassel core component
%description core
The Quassel IRC Core maintains a connection with the
server, and allows for multiple clients to connect

%package core-lib
Summary: Libquassel core
%description core-lib
LibQuassel core

%package client
Summary: Quassel client
Requires: %{name}-common = %{version}-%{release}
Requires: %{name}-client-lib = %{version}-%{release}
%description client
Quassel client

%package client-lib
Summary: Libquassel client
%description client-lib
LibQuassel client

%prep
%autosetup -n quassel
# cd %{_topdir}/BUILD
# rm -rf %{name}-%{version}
# cp -rf %{_topdir}/SOURCES/%{name}-%{version} .
# cd %{name}-%{version}
# /usr/bin/chmod -Rf a+rX,u+w,g-w,o-w .

%build
mkdir build
pushd build
%cmake .. -DWANT_MONO=1 -DUSE_QT5=1 -DWITH_KDE=1 -DHAVE_SSL=1
%make_build
popd

%install
pushd build
%make_install
popd

%files
%{_datadir}/applications/quassel.desktop 
%{_bindir}/quassel
%license gpl-3.0.txt

%files common
%{_datadir}
%exclude %{_datadir}/applications/quassel.desktop 
%exclude %{_datadir}/applications/quasselclient.desktop

%files common-lib
%{_libdir}/libquassel-common.so*

%files core
%{_bindir}/quasselcore

%files core-lib
%{_libdir}/libquassel-core.so*

%files client
%{_datadir}/applications/quasselclient.desktop 
%{_bindir}/quasselclient

%files client-lib
%{_libdir}/libquassel-client.so*
%{_libdir}/libquassel-qtui.so*
%{_libdir}/libquassel-uisupport.so*

%changelog
* Sun Feb  2 2020 Max Audron <audron@cocaine.farm>
- 
